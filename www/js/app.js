// Ionic Starter App
// cordova plugin add https://github.com/MobileChromeApps/cordova-plugin-chrome-apps-sockets-tcpServer.git
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'

var tcpServer = null;
var socketId = null;
var server_ip = '192.168.1.10';

function ab2str(buf) {
  return String.fromCharCode.apply(null, new Uint16Array(buf));
}
function str2ab(str) {
  var buf = new ArrayBuffer(str.length*2); // 2 bytes for each char
  var bufView = new Uint16Array(buf);
  for (var i=0, strLen=str.length; i < strLen; i++) {
    bufView[i] = str.charCodeAt(i);
  }
  return buf;
}

angular.module('starter', ['ionic'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
      // tcpServer = chrome.sockets.tcpServer;
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

// .controller('mainCtrl', function($scope, $ionicPlatform){
//   console.log('mainCtrl');
//   // console.log('sockets.tcpServer', sockets.tcpServer);
//   // console.log('ChromeSocketsTcpServer', ChromeSocketsTcpServer);
//   $scope.start_one = function(){
//     console.log('tcpServer', tcpServer);
//   }
//
//   $scope.socket_create = function(){
//     tcpServer.create({
//       persistent: false,
//       name: 'tcp_socket_server_one',
//     }, function(createInfo){
//       console.log('createInfo', createInfo);
//       // Listening TCP server
//       socketId = createInfo.socketId;
//       tcpServer.listen(createInfo.socketId, "192.168.1.10", 8000, function(result) {
//           // ...
//           console.log('listen', result);
//       });
//     });
//   }
// // 192.168.1.10
//
//   $scope.socket_close = function(){
//     tcpServer.disconnect(socketId, function(disc_info){
//       console.log('disconnect', disc_info);
//       tcpServer.close(socketId, function(info){
//         console.log('socket_close', info);
//       });
//     });
//   }
//
//   $scope.get_info = function(){
//     tcpServer.getInfo(socketId, function(info){
//       console.log('get_info', info);
//     });
//   }
//
//   $scope.get_sockets = function(){
//     tcpServer.getSockets(function(socketInfos){
//       console.log("socketInfos", socketInfos);
//     });
//   }
//
//   $ionicPlatform.ready(function(){
//     tcpServer.onAccept.addListener(function(info){
//       if (info.socketId != socketId)
//         return;
//
//       console.log('onAccept', info);
//       // chrome.sockets.tcp.send(info.clientSocketId, data,
//       //   function(resultCode) {
//       //     console.log("Data sent to new TCP client connection.")
//       // });
//
//       // Start receiving data.
//       chrome.sockets.tcp.onReceive.addListener(function(recvInfo) {
//         console.log('onReceive', recvInfo);
//         if (recvInfo.socketId != info.clientSocketId)
//           return;
//         // recvInfo.data is an arrayBuffer.
//         console.log('recvInfo.data', recvInfo.data);
//         console.log('recvInfo.data String ==== ', ab2str(recvInfo.data));
//       });
//       chrome.sockets.tcp.setPaused(false);
//     });
//   });
//
//
// })


.controller('mainCtrl', function($scope, $ionicPlatform){
  console.log('mainCtrl');
  $scope.status_log = 'status';
  $ionicPlatform.ready(onReady);

  function onReady(){
    networkinterface.getIPAddress(function(server_ip){
      console.log('server_ip', server_ip);
      $scope.$apply(function(){
        $scope.server_ip = server_ip;
      });
      chrome.sockets.tcpServer.create({}, function(createInfo) {
        listenAndAccept(createInfo.socketId, server_ip);
      });
    }, function(info){
      console.log('Error getIPAddress', info);
    });

    // chrome.sockets.tcpServer.create({}, function(createInfo) {
    //   listenAndAccept(createInfo.socketId, server_ip);
    // });

  }

  function listenAndAccept(socketId, server_ip) {
    console.log('listenAndAccept', socketId, server_ip);
    console.log('typeof ', typeof server_ip);
    chrome.sockets.tcpServer.listen(socketId,
      server_ip, 8000, function(resultCode) {
        $scope.$apply(function(){
          $scope.status_log = 'listen' + resultCode;
        });
        onListenCallback(socketId, resultCode);
    }, function(error){
      console.log('Error listen', error);
    });
  }

  var serverSocketId;
  function onListenCallback(socketId, resultCode) {
    console.log('onListenCallback', socketId, resultCode);
    if (resultCode < 0) {
      console.log("Error listening:" +
      chrome.runtime.lastError.message);
      return;
    }
    serverSocketId = socketId;
    chrome.sockets.tcpServer.onAccept.addListener(onAccept);
  }

  function onAccept(info) {
    console.log('onAccept', info);
    if (info.socketId != serverSocketId)
      return;

    // A new TCP connection has been established.
    var data = str2ab("Data sent to new TCP client connection.");
    chrome.sockets.tcp.send(info.clientSocketId, data,
      function(resultCode) {
        console.log("Data sent to new TCP client connection.")
    });

    // Start receiving data.
    chrome.sockets.tcp.onReceive.addListener(function(recvInfo) {
      console.log('onReceive', recvInfo);
      var rec_data = ab2str(recvInfo.data);
      console.log('onReceiveStr', rec_data);
      $scope.$apply(function(){
        $scope.rec_data = rec_data;
      });
      if (recvInfo.socketId != info.clientSocketId)
        return;
      // recvInfo.data is an arrayBuffer.
    });
    chrome.sockets.tcp.onReceiveError.addListener(function(info) {
      console.log("onReceiveError: ", info);
    });
    chrome.sockets.tcp.setPaused(info.clientSocketId, false);
  }



});
